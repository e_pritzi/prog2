#include <stdio.h>
#include <stdlib.h>

// function declarations
long *alloc_number(long size);
void free_number(long *mem);
void print(long *mem, long size);
long *change_size(long *mem, long newSize);
char ** store_in_array(char **storage, long length, char *newitem);


#define SIZE 5

int main()
{
    long * myField = alloc_number(SIZE);
    if(myField == NULL)
        return 1;

    long i;
    for(i = 0; i < SIZE; i++)
    {
        myField[i] = i;
    }

    print(myField, SIZE);

    free_number(myField);


    return 0;
}


long *alloc_number(long size)
{
    long * myField = malloc(size*sizeof(long));
    return myField;
}

void free_number(long *mem)
{
    free(mem);
}

void print(long *mem, long size)
{
    // print in reverse order
    long i;
    for(i = size-1; i >= 0; i--)
        printf("%ld\n", mem[i]);
}

long *change_size(long *mem, long newSize)
{
    return realloc(mem, newSize);
}

char **store_in_array(char **storage, long length, char *newitem)
{
    char **tmpPtr = realloc(storage, length+1);
    if(tmpPtr == NULL)
        return NULL;

    tmpPtr[length] = newitem;
    return tmpPtr;
}

char **delete_from_array(char **storage, long length, long delete_index)
{
    long i;
    for(i = 0; i < length; i++)
    {
        if(i > delete_index)
        {
            storage[i-1] = storage[i];
        }
    }

    storage[length-1] = NULL;

    return storage;
}
