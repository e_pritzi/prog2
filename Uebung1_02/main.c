#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// DEFINES


// Funktionsdeklarationen
double * allocate_double(long len);
void free_double(double *mem);
void print(double *mem, long len);
double * double_size(double *zahl, long old_size);
long sub(double *a, long size1, double *b, long size2, double *c, long size3);
char ** store_in_array(char ** storage, long length, char * newitem);
char **delete_from_array(char ** storage, long length, long delete_index);
long sortCharArray(char ** storage, long length, int (*cmpFct)(const char * a, const char * b));

int main()
{
    //long length = 10;

    /*double * dynArray = allocate_double(length);
    double * dynArrayb = allocate_double(length);
    double * dynArrayc = allocate_double(length);
    if(dynArray == NULL || dynArrayb == NULL || dynArrayc == NULL)
    {
        if(dynArray != NULL)
            free_double(dynArray);
        if(dynArrayb != NULL)
            free_double(dynArrayb);
        if(dynArrayc != NULL)
            free_double(dynArrayc);
        return 1;
    }


    long i;
    for(i = 0; i < length; i++)
    {
        dynArray[i] = i * 2.31;
        dynArrayb[i] = i * 1.25;
    }

    print(dynArray, length);
    printf("\n");
    print(dynArrayb, length);
    sub(dynArray, length, dynArrayb, length, dynArrayc, length);

    printf("\n");
    print(dynArrayc, length);


    double *tmp;
    if((tmp = double_size(dynArrayc, length)) == NULL)
        return -1;

    dynArrayc = tmp;
    long lengthC = 2 * length;

    printf("\n");
    print(dynArrayc, lengthC);

    free_double(dynArray);
    free_double(dynArrayb);
    free_double(dynArrayc);
    */




    char * b1 = "just";

    char ** storage = malloc(4 * sizeof(char *));

    storage[0]= "hallo";
    storage[1]= "das";
    storage[2]= "ist";
    storage[3]= "ein";
    storage[4]= "Test";


    long i;
    for(i = 0; i < 4; i++)
    {
        printf("%s\n", storage[i]);
    }

    char ** tmp = store_in_array(storage, 4, b1);

    if(tmp !=NULL)
        storage = tmp;

    printf("\n");

    for(i = 0; i < 5; i++)
    {
        printf("%s\n", storage[i]);
    }

    tmp = delete_from_array(storage, 5, 2);

    if(tmp !=NULL)
        storage = tmp;

    printf("\n");

    for(i = 0; i < 4; i++)
    {
        printf("%s\n", storage[i]);
    }

    sortCharArray(storage, 4, &strcmp);

    printf("\n");
    for(i = 0; i < 4; i++)
    {
        printf("%s\n", storage[i]);
    }

    free(storage);

    return 0;
}

/*
    This function allocates memory for len doubles
*/
double * allocate_double(long len)
{
    double * myPointer = malloc(sizeof(double) * len);

    return myPointer;
}


/*
    This functions frees the allocated memory
*/
void free_double(double *mem)
{
    free(mem);
}

/*
    This functions prints the content of the dynamic field
*/
void print(double * mem, long len)
{
    long i;
    for(i = 0; i < len; i++)
    {
        printf("%g\n", mem[i]);
    }
}


/*
    This function doubles the number of elements
    Returns NULL in case of error
*/
double * double_size(double * zahl, long old_size)
{
    return realloc(zahl, (2 * old_size)*sizeof(double));
}


/*

*/
long sub(double * a, long size1, double * b, long size2, double * c, long size3)
{
    // keine Ahnung welche genauen Ueberpruefungen verlangt werden?
    //alternativ realloc
    if(a == NULL || b == NULL || c == NULL)
        return -1;

    if(size1 == size2 && size3 >= size1)
    {
        long i;
        for(i = 0; i < size1; i++)
        {
            c[i] = a[i] - b[i];
        }
        return 0;
    }

    return -2;
}

char ** store_in_array(char ** storage, long length, char * newitem)
{
    char ** tmp;
    if((tmp = realloc(storage, sizeof(char *) * (length + 1))) == NULL)
        return NULL;

    tmp[length] = newitem;

    return tmp;
}


char **delete_from_array(char ** storage, long length, long delete_index)
{
    if(delete_index >= length)
        return NULL;

    // free content of the element at delete_index
    // free(storage[delete_index]);

    long i;
    for(i = delete_index; i < length - 1; i++)
        storage[i] = storage[i+1];

    storage[length-1] = NULL;

    return realloc(storage, sizeof(char*) * (length - 1));
}


long sortCharArray(char ** storage, long length, int (*cmpFct)(const char *, const char *))
{
    long i, j;
    for(i = length - 1; i > 0; i--)
    {
        for(j = 0; j < i; j++)
        {
            if((*cmpFct)(storage[j], storage[j+1]) > 0)
            {
                // change position
                char * tmp = storage[j];
                storage[j] = storage[j+1];
                storage[j+1] = tmp;
            }
        }
    }

    return 0;
}
